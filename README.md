# Trabalho Compiladores

Alunos: Arthur Cardoso, Carlos Said e Giugliano Severo.

Para rodar scan(Teste de lexer):
java -jar /compilador/trab/compilador/dist/Compiler.jar -target scan /compilador/trab/scanner/charX
 
Para rodar parse (Teste de parser):
java -jar /compilador/trab/compilador/dist/Compiler.jar -target parse /compilador/trab/parser/illegal-01

### Testes
Saídas que já foram testadas e estão de acordo com o arquivo de output
```
LEXER:
char1: X
char2: X
char3: ?
char4: ?
char6: 
char7: 
char8: 
char9: 
hexlit1: X
hexlit2: X
hexlit3: X
id1: X
id2: X
id3: X
number1: X
number2: X
op1: X
op2: X
string1: X
string2: 
string3: X
tokens1: X
tokens2: X
tokens3: X
tokens4: 
ws1: X

PARSER
illegal-01: X
illegal-02:
illegal-03: X
illegal-04:
illegal-05:
illegal-06:
illegal-07:
illegal-08:
illegal-09:
illegal-10:
illegal-11:
illegal-12:
illegal-13:
illegal-14:
illegal-15:
illegal-16:
illegal-17:
illegal-18:
illegal-19:
illegal-20:
legal-01:
legal-02:
legal-03:
legal-04:
legal-05:
legal-06:
legal-07:
legal-08:
legal-09:
legal-10:
legal-11:
legal-12:
legal-13:
legal-14:
legal-15:
legal-16:
legal-17:
```

Regras 
```
1. ID são únicos no mesmo escopo
2. Nenhum ID pode ser usado antes de declarado
3. Existe um método main que não possui parâmetros
4. No array de tamanho (int) temque ser maior que vazio
5. A definição de parâmetros do método deve ser igual a chamada (assinatura)
6. Se a chamada do método é utilizada numa expressão, ele precisa retornar um valor
7. Um método tem que retornar o mesmo tipo declarado
8. 

```
